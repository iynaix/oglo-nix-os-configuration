# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  lib,
  inputs,
  system,
  config,
  pkgs,
  username,
  ...
}:
# Variables
let
  fullname = "Jackson Novak";

  editor = "hx";
  browser = "brave";
in {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "Linux"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.${username} = {
    isNormalUser = true;
    description = "${fullname}";
    extraGroups = ["networkmanager" "wheel" "video" "kvm"];
    packages = with pkgs; [];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    #### Core Packages
    wget
    procps
    killall
    zip
    unzip
    bluez
    bluez-tools
    libnotify
    brightnessctl
    light
    xdg-utils

    #### Standard Packages
    networkmanager
    networkmanagerapplet
    git
    vim
    tldr
    pipewire
    sox
    yad

    #### My Packages
    helix
    brave
    cinnamon.nemo-with-extensions
    lightdm
    kitty
    bat
    exa
    tmux
    pavucontrol
    blueman
    trash-cli
    ydotool
    cava
    neofetch

    #### My Proprietary Packages
    discord
    steam

    #### Hyprland Rice
    hyprland
    xwayland
    cliphist
    alacritty
    rofi-wayland
    swww
    swaynotificationcenter
    papirus-icon-theme
    lxde.lxsession
    # missing: grimblast
    # missing: nwg-look
    cinnamon.mint-themes
    # missing: xcursor-simp1e-gruvbox-light
    gtklock
    eww-wayland
    xdg-desktop-portal-hyprland
  ];

  # Font stuff:
  fonts.fontDir.enable = true;
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-emoji
    noto-fonts-cjk
    (nerdfonts.override {fonts = ["JetBrainsMono"];})
  ];

  # Environment variables:
  environment.variables = {
    STARSHIP_CONFIG = "/home/${username}/.config/starship/starship.toml";
    EDITOR = "${editor}";
    BROWSER = "${browser}";
  };

  ## Enable some shit:
  # Programs
  programs.hyprland = {
    enable = true;
    xwayland = {
      enable = true;
      hidpi = true;
    };
  };

  # Services
  services.printing = {
    enable = true;
  };

  # Hardware
  hardware.bluetooth.enable = true;
  ## ^

  services.xserver = {
    layout = "us";
    xkbVariant = "";
    enable = true;
    libinput.enable = true;
    displayManager.lightdm = {
      enable = true;
    };
  };

  services.locate = {
    enable = true;
  };

  services.blueman.enable = true;
  services.devmon.enable = true;
  services.udisks2.enable = true;
  services.dbus.enable = true;

  # XDG Desktop Portal stuff
  xdg.portal = {
    enable = true;
  };

  # Sound
  security.rtkit.enable = true;
  sound.enable = lib.mkForce false;
  hardware.pulseaudio.enable = lib.mkForce false;
  services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse.enable = true;
    wireplumber.enable = true;
    # jack.enable = true; # (optional)
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Home Manager Options
  home-manager.users.${username} = {
    programs.waybar = {
      enable = true;
      package = inputs.hyprland.packages.${system}.waybar-hyprland;
    };
  };

  # Package overlays:
  nixpkgs.overlays = [
    (self: super: {
    })
  ];

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
